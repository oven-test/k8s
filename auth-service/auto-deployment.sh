#!/bin/sh

TAG=$1

cat <<EOF > deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: auth-api
  namespace: prod
  labels:
    app: auth-api
    tier: auth-api
spec:
  replicas: 1
  selector:
    matchLabels:
      app: auth
  template:
    metadata:
      namespace: kong
      name: auth-pod
      labels:
        app: auth
    spec:
      containers:
        - name: auth-service
          image: khuong02/oven-auth-service:$TAG
          imagePullPolicy: Always
          ports:
            - containerPort: 9000
          envFrom:
            - secretRef:
                name: auth-secret
            - configMapRef:
                name: auth-service-config
          resources:
            requests:
              memory: "10Mi"
              cpu: "100m"
            limits:
              memory: "30Mi"
              cpu: "300m"
EOF
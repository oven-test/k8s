#!/bin/bash

function _help_menu() {
    # Help menu
    echo '[*] Help Menu:'
    echo ''
    echo '[*] --auth-service    Deploy auth-service.'
    echo ''
    echo '[*] --todo-service    Deploy todo-service.'
    echo ''
    exit
}

function deploy_auth_service() {
    cd ./auth-service && chmod +x auto-deployment.sh
    ./auto-deployment.sh $1 && k3s kubectl apply -f deployment.yaml
}

function deploy_todo_service() {
    cd ./todo-service && chmod +x auto-deployment.sh
    ./auto-deployment.sh $1 && k3s kubectl apply -f deployment.yaml
}

case "$1" in
    --auth-service)
        deploy_auth_service $2
        ;;
    --todo-service)
        deploy_todo_service $2
        ;;
    -h)
        _help_menu
        ;;
    --help)
        _help_menu
        ;;
    *)
        _help_menu
        ;;
esac


#!/bin/sh

TAG=$1

cat <<EOF > deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: todo-api
  namespace: prod
  labels:
    app: todo-api
    tier: todo-api
spec:
  replicas: 1
  selector:
    matchLabels:
      app: todo
  template:
    metadata:
      namespace: kong
      name: todo-pod
      labels:
        app: todo
    spec:
      containers:
        - name: todo-service
          image: khuong02/oven-todo-service:$TAG
          imagePullPolicy: Always
          ports:
            - containerPort: 9000
          envFrom:
            - secretRef:
                name: todo-secret
            - configMapRef:
                name: todo-service-config
          resources:
            requests:
              memory: "10Mi"
              cpu: "100m"
            limits:
              memory: "30Mi"
              cpu: "300m"
EOF